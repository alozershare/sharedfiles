Kullanım Dökümanı

* Veritabanı bilgilerinin "app.properties" dosyasına yazılması gerekmektedir.
    spring.datasource.url=<url>
    spring.datasource.username=<username>
    spring.datasource.password=<password>

* Uygulamayı çalıştırmak için aşağıdaki komutun çalıştırılması gerekmektedir.
    java -jar lease-cockpit-1.5.0.jar --spring.config.location=./app.properties

* Örnek veri yüklemek için "app.properties" dosyasındaki "Sample Data" bloğu yorum satırları açılabilir.

* Uygulama loglarına bakmak için otomatik oluşturulan "log" klasörüne bakılabilir.

* Uygulama TFKB-Demo versiyonunda olduğu için 01/12/2018 tarihine kadar çalışacaktır. 

* Bir önceki lisans kısıtlaması(20 sözleşme sınırı) kaldırılmıştır.


1.5.0 versiyonu için yapılması gerekenler
=========================================

* 1.4.0 versiyonu ile gelen 2 output tablosunun silinmesi gerekmektedir.
  ** dailyCalculations
  ** monthlyCalculations

* 1.5.0 versiyonu için gereken 'calculations' tablosunun oluşturulması gerekmektedir.
	CREATE TABLE calculations(
		id NUMBER(10) PRIMARY KEY NOT NULL,
		contractId NUMBER(10) NOT NULL,
		calculationDate DATE NOT NULL,
		yearBegin DATE NOT NULL,
		monthBegin DATE NOT NULL,
		liability DECIMAL NOT NULL,
		rtuAsset DECIMAL NOT NULL,
		liabilityDiff DECIMAL NOT NULL,
		dailyDepreciation DECIMAL NOT NULL,
		monthlyDepreciation DECIMAL NOT NULL,
		yearlyDepreciation DECIMAL NOT NULL,
		accumulatedDepreciation DECIMAL NOT NULL,
		dailyFinExpense	DECIMAL NOT NULL,
		monthlyFinExpense DECIMAL NOT NULL,
		yearlyFinExpense DECIMAL NOT NULL,
		accumulatedFinExpense DECIMAL NOT NULL,
		dailyFxIncome DECIMAL NOT NULL,
		monthlyFxIncome DECIMAL NOT NULL,
		yearlyFxIncome DECIMAL NOT NULL,
		accumulatedFxIncome DECIMAL NOT NULL,
		dailyCashOutflow DECIMAL NOT NULL,
		monthlyCashOutflow DECIMAL NOT NULL,
		yearlyCashOutflow DECIMAL NOT NULL,
		accumulatedCashOutflow DECIMAL NOT NULL,
		remainingCashOutflow DECIMAL NOT NULL,
		CONSTRAINT calculations_fk FOREIGN KEY(contractId) REFERENCES contracts(id) ON DELETE CASCADE
	);